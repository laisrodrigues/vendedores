<?php

include_once 'Vendedor.php';
include_once 'vendedorDAO.php';

$acao = $_REQUEST['acao'];

if($acao=='inserir'){
    $nome = $_REQUEST['vendedor'];
    $obj = new Vendedor();
    $obj->setNome($nome);
    
    $dao = new vendedorDAO();
    $inseriu = $dao->inserir($obj);
    if($inseriu){
        echo 'Cadastrado com sucesso';
        header('Location: index.php');
    }else{
        echo 'Erro ao cadastrar';
    }
}

<?php

include_once 'conexao.php';
include_once 'Vendedor.php';

class vendedorDAO extends conexao {

    public $connection = NULL;

    public function __construct() {
        $conexao = new Conexao();
        $this->connection = $conexao->conecta();
    }

    function inserir(Vendedor $vendedor) {
        try {
            $stmt = $this->connection->prepare('INSERT INTO vendedores (nome) VALUES(:nome)');
            $stmt->execute(array(
                ':nome' => $vendedor->getNome()
            ));
            if($stmt->rowCount()>=1){
                return true;
            }else{
                return false;
            }
        } catch (Exception $ex) {
            
        }
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

